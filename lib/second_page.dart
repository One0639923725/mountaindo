import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  SecondPage({this.nama, this.gambar});
  final String nama;
  final String gambar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new ListView(
        children: <Widget>[
          new Container(
            height: 250.0,
            child: new Hero(
                tag: nama,
                child: new Material(
                  child: new InkWell(
                      child: new Image.asset(
                    "assets/img/$gambar",
                    fit: BoxFit.cover,
                  )
                  ),
                )),
          ),
          new NamaApp(
            nama: nama,
          ),
          new Keterangan()
        ],
      ),
    );
  }
}

class NamaApp extends StatelessWidget {
  NamaApp({this.nama});
  final String nama;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Text(
                  nama,
                  style: new TextStyle(fontSize: 20.0, color: Colors.black),
                ),
              ],
            ),
          ),
          new Row(
            children: <Widget>[
              new Icon(
                Icons.favorite,
                size: 40.0,
                color: Colors.red,
              )
            ],
          ),
        ],
      ),
    );
  }
}

class Keterangan extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Card(
        child:
            new Padding(
                padding: const EdgeInsets.all(15.0),
              child:
              new Text(
              "Padang rumput serta pepohonan yang hijau yang sesekali dihiasi awan putih menjadi sebuah sajian yang akan membuat kita selalu rindu dengan suasana pendakian",
              style: new TextStyle(fontSize: 16.0), textAlign: TextAlign.justify,),
            ),

      ),
    );
  }
}
