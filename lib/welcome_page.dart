import 'package:flutter/material.dart';
import 'package:mountaindo/home_page.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {

  String value;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new TextFormField(
              autofocus: false,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                hintText: "Nama",
                labelText: "Masukkan Nama",
              ),
              keyboardType: TextInputType.name,
              onChanged: (text){
                value = text;
              },

            ),
            new Padding(padding: new EdgeInsets.all(10.0)),
            new Container(
              child: RaisedButton(
                child: Text("Started"),
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (context) => HomePage(value : value),
                  )
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
